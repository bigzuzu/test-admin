import { Component, OnInit } from '@angular/core';
import { testModulesAndRoots, RolesService, IRole } from '@services/Roles.service';
import { RippleRef } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { AddRoleModalComponent } from './../add-role-modal/add-role-modal.component';
import { AddModulesModalComponent } from './../add-modules-modal/add-modules-modal.component'
import { identifierModuleUrl } from '@angular/compiler';

@Component({
    selector: 'app-roles-list',
    templateUrl: './roles-list.component.html',
    styleUrls: ['./roles-list.component.scss']
})

export class RolesListComponent implements OnInit{
    public checkRoot: boolean[] = [];
    public modules = testModulesAndRoots;
    public roles: IRole[] = [];
    form: any = {};

    public currentModule;
    public currentRole;

    constructor(
        public dialog: MatDialog,
        private rolesService: RolesService
    ) {
    }

    async ngOnInit() {
        this.roles = await this.rolesService.loadRoles()
        this.prepareForm()
    }

    chooseModule(module: any) {
        this.currentModule = module
    }

    showModules(role: any) {
        this.currentRole = role;
    }

    openAddModal() {
        const dialogRef = this.dialog.open(AddRoleModalComponent, {
            width: '550px',
            data: {name: 'города', value: ''}
        });
    }

    openAddModuleModal() {
        const dialogRef = this.dialog.open(AddModulesModalComponent, {
            width: '550px',
            data: {name: 'города', value: ''}
        });
    }

    prepareForm() {
        this.modules.forEach((module) => {
            this.form[module.id] = module.rules.map(item => ({id: item.ruleId, isActive: false}))
        })
    }
}