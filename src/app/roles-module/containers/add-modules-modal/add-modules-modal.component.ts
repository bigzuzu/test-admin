import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'add-modules-modal',
    templateUrl: './add-modules-modal.component.html',
    styleUrls: ['./add-modules-modal.component.scss']
})

export class AddModulesModalComponent implements OnInit {
    
    constructor(
        public dialogRef: MatDialogRef<AddModulesModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    ngOnInit() {
    }
}