import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'add-role-modal',
    templateUrl: './add-role-modal.component.html',
    styleUrls: ['./add-role-modal.component.scss']
})

export class AddRoleModalComponent implements OnInit {
    
    constructor(
        public dialogRef: MatDialogRef<AddRoleModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    ngOnInit() {
    }
}