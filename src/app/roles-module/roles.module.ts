import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RolesListComponent } from './containers/roles-list/roles-list.component';
import { AddRoleModalComponent } from './containers/add-role-modal/add-role-modal.component';
import { AddModulesModalComponent } from './containers/add-modules-modal/add-modules-modal.component'

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule,
        MatTableModule,
        MatSelectModule,
        MatCheckboxModule
    ],
    declarations: [
        RolesListComponent,
        AddRoleModalComponent,
        AddModulesModalComponent
    ],
    providers: [
    ]
})
export class RolesModule {

}