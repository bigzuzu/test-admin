import { Route } from '@angular/router';
import { RolesListComponent } from './containers/roles-list/roles-list.component';

export const routes: Route[] = [
    { path: '', component: RolesListComponent }
]