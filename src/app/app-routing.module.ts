import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout/containers/admin-layout.component';
import { AuthGuard } from "@services/AuthGuard.service";

export const routes: Routes = [
  { path: '', loadChildren: () => import( './auth-module/auth.module').then(m => m.AuthModule), data: {preload: true} },
  { path: '', component: AdminLayoutComponent, canActivate: [AuthGuard], children: [
    {
      path: 'users', 
      loadChildren: () => import('./users-module/users.module').then(m => m.UsersModule)
    },
    {
      path: 'roles', 
      loadChildren: () => import('./roles-module/roles.module').then(m => m.RolesModule)
    },
    {
      path: 'filials',
      loadChildren: () => import ('./filials-module/filials.module').then(m => m.FilialsModule)
    }
  ]}
];