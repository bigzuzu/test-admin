import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '@services/Auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public form: any = {
      email: '',
      password: ''
  };

  @ViewChild('loginForm', { read: NgForm, static: true })
  private loginForm: NgForm;

  public isAuthInProgress = false;
  public authError = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  submit() {
    let loginForm = {
      login: this.form.email,
      password: this.form.password
    }
    this.authService.login(loginForm).then(() => {
      this.router.navigate(['/'])
    })
  }

}
