import { Route } from '@angular/router';
import { LoginComponent } from './containers/login/login.component';
export const routes: Route[] = [
    {path: 'login', component: LoginComponent, children: []}
]