import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddFilialsModalComponent } from '../add-filials-modal/add-filials-modal.component';
import { FilialsService, IFilial } from '@services/Filials.service';

@Component({
    selector: 'filials-list',
    templateUrl: './filials-list.component.html',
    styleUrls: ['./filials-list.component.scss']
})

export class FilialsListComponent {
    public filials: IFilial[] = [];

    constructor(
        public dialog: MatDialog,
        private filialsService: FilialsService
        ) {}

    async ngOnInit() {
        this.filials = await this.filialsService.loadFilials()
    }

    openAddFilialModal() {
        const dialogRef = this.dialog.open(AddFilialsModalComponent, {
            width: '550px',
            data: { value: ''}
        });
    }
}