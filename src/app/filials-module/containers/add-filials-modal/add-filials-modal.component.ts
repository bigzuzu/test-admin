import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'add-filials-modal',
    templateUrl: './add-filials-modal.component.html',
    styleUrls: ['./add-filials-modal.component.scss']
})

export class AddFilialsModalComponent implements OnInit {
    
    constructor(
        public dialogRef: MatDialogRef<AddFilialsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

    ngOnInit() {
    }
}