import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FilialsListComponent } from './containers/filials-list/filials-list.component';
import { AddFilialsModalComponent } from './containers/add-filials-modal/add-filials-modal.component';

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        FormsModule,
        InlineSVGModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDialogModule
    ],
    declarations: [
        FilialsListComponent,
        AddFilialsModalComponent
    ],
    providers: [
    ]
})
export class FilialsModule {

}