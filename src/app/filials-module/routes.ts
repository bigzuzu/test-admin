import { Route } from '@angular/router';
import { FilialsListComponent } from './containers/filials-list/filials-list.component';

export const routes: Route[] = [
    { path: '', component: FilialsListComponent}
]