import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IUserForm } from '@services/Users.service';
import { NgForm } from '@angular/forms';
import { FilialsService } from '@services/Filials.service';
import { RolesService } from '@services/Roles.service';

@Component({
    selector: 'add-modal',
    templateUrl: './add-modal.component.html',
    styleUrls: ['./add-modal.component.scss']
})

export class AddModalComponent implements OnInit {
    public officeSelectOpts: any[] = [];
    public rolesSelectOpts: any[] = []
    @Output()
    public submitForm = new EventEmitter<IUserForm>();

    constructor(
        public dialogRef: MatDialogRef<AddModalComponent>,
        private filialsService: FilialsService,
        private rolesService: RolesService,
        @Inject(MAT_DIALOG_DATA) public data: IUserForm
    ) {}

    async ngOnInit() {
        this.officeSelectOpts = this.filialsService.filials.map((item) => ({value: item.filialID, label: item.filialNameShort}))
        let roles = await this.rolesService.loadRoles();
        this.rolesSelectOpts = roles.map((item) => ({value: item.roleID, label: item.roleName}))
    }

    submit(ngForm: NgForm) {
        if(!ngForm.valid) {
            return
        }
        this.submitForm.emit(this.data);
        this.dialogRef.close()
    }
}