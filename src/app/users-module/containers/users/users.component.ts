import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddModalComponent } from '../add-modal/add-modal.component';
import { UserService, defaultUserForm, ITransformedUsers } from '@services/Users.service';
import { AuthService } from '@services/Auth.service';
import { ToastrService } from 'ngx-toastr';
import { DateTime } from 'luxon';
import { DatePipe } from '@angular/common';
import { RolesService, IRole } from '@services/Roles.service';
import { ActivatedRoute } from '@angular/router';
import { FilialsService, IFilial } from '@services/Filials.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
    public usersList: ITransformedUsers[] = [];
    public role: boolean = false;
    public activeUser: ITransformedUsers;
    public roleList: IRole[] = [];
    private pipe = new DatePipe('en-US');
    public filialId: number = 0;
    public currentFilial: IFilial;
    public activeRoles: boolean[] = [];
    
    constructor(
        public dialog: MatDialog,
        private userService: UserService,
        private authService: AuthService,
        private rolesService: RolesService,
        private filialsService: FilialsService,
        public toastr: ToastrService,
        private _activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this._activatedRoute.paramMap.subscribe((v) => {
            this.filialId = parseInt(v.get('id'))
            this.activeUser = null;
            this.currentFilial = this.filialsService.filials.find((item) => item.filialID ==  this.filialId);
            this.updateList();
        })
    }

    // загружаем список пользователей и ролей
    async updateList() {
        this.usersList = await this.userService.loadUsers(this.authService.user.userID, this.filialId);
        this.roleList = await this.rolesService.loadRoles();
    }

    // создаем список состояния ролей, апи не отдает поле роль в айтеме юзера, поэтому делаем отдельно
    updateRolesStaus() {
        this.roleList.forEach((item) => {
            if(this.activeUser.roleIDs.find((role) => role == item.roleID)) {
                this.activeRoles[item.roleID] = true;
            } else {
                this.activeRoles[item.roleID] = false;
            }
        })
    }

    // открывается модалка с формой добавления пользователя
    openAddModal() {
        const dialogRef = this.dialog.open(AddModalComponent, {
            width: '750px',
            data: defaultUserForm
        });
        dialogRef.componentInstance.submitForm.subscribe((data) => {
            let dateBirth = this.pipe.transform(DateTime.fromJSDate(data.dateBirth).toISO(), 'yyyy-MM-dd');
            this.userService.addUser(data, dateBirth).then((res) => {
                if(res.ok) {
                    this.userService.setRoleAndFilial(data.filialId, res.userNewID, data.role);
                    this.toastr.success(res.msg);
                    this.updateList();
                } else {
                    this.toastr.error(res.msg);
                }                  
            })
        })
    }

    // сохраняем пользователю роль выбранную чекбоксом
    saveChanges(id: number) {
        if(this.activeRoles[id]) {
            this.userService.setRoleAndFilial(this.activeUser.filialID, this.activeUser.userID, id);
        } else {
            this.userService.deleteRole(this.activeUser.filialID, this.activeUser.userID, id);
        }
        this.updateList()
    } 

    showRoles(user: any) {
        this.activeUser = user;
        this.updateRolesStaus();
    } 
}