import { Route } from '@angular/router';
import { UsersComponent } from './containers/users/users.component';

export const routes: Route[] = [
    { path: ':id', component: UsersComponent }
]