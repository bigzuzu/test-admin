import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

export interface IFilial {
    filialID: number;
    filialNameShort: string; 
    filialNameAll: string;
    cityID: number;
}

@Injectable({
    providedIn: 'root'
})
export class FilialsService {
    public filials: IFilial[] = [];

    constructor(
        private _http: HttpClient
    ) {
    }

    // загрузка списка подразделений
    async loadFilials(): Promise<IFilial[]> {
        return this._http.get<IFilial[]>(`api/FilialsList`)
            .toPromise()
            .then((response) => {
                this.filials = response
                return response
            })
    }

}