import { Injectable, Injector } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

export interface ILoginForm {
    login: string;
    password: string;
}

export interface IAdminUser {
    filialID: number;
    filialNameAll: string;
    filialNameShort: string;
    postID: number;
    postName: string;
    roleID: number;
    roleName: string;
    userFioAll: string;
    userFioShort: string;
    userID: number;
}


@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public user: IAdminUser;
    private _isAuthenticated$$ = new BehaviorSubject(false);
    public isAuthenticated$ = this._isAuthenticated$$.asObservable();

    constructor(
        private _http: HttpClient,
        private _injector: Injector
    ) {
    }

    public login(form: ILoginForm) {
        return this._http.get<IAdminUser[]>(`/api/Login?UserLogin=${form.login}&UserPass=${form.password}`)
            .toPromise()
            .then((response) => {
                this.user = response[0]
                this._isAuthenticated$$.next(true);
            })
    }

    public async logout() {
        return this._http.post<any>(`api/Logout?UserID=${this.user.userID}`, {})
            .toPromise()
            .then((response) => {
                this._isAuthenticated$$.next(false);
                this._injector.get(Router).navigate(['/login']);
            })
    }

    // проверка авторизован ли пользователь
    public isAuthenticated() {
        return this._isAuthenticated$$.value;
    }
}