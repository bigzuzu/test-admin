import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

export const testModulesAndRoots = [
    {
        moduleName: "Мониторинг",
        id: 1,
        rules: [
            {
                ruleName: "запуск модуля",
                ruleId: 1
            },
            {
                ruleName: "создание предложения",
                ruleId: 2
            },
            {
                ruleName: "редактирование предложения",
                ruleId: 3
            }
        ]
    },
    {
        moduleName: "Иск",
        id: 2,
        rules: [
            {
                ruleName: "запуск модуля",
                ruleId: 1
            },
            {
                ruleName: "создание иска ",
                ruleId: 2
            },
            {
                ruleName: "редактирование иска",
                ruleId: 3
            }
        ]
    },
    {
        moduleName: "Портал правового информирования",
        id: 3,
        rules: [
            {
                ruleName: "запуск модуля",
                ruleId: 1
            },
            {
                ruleName: "просмотр новостей",
                ruleId: 2
            },
            {
                ruleName: "просмотр файла",
                ruleId: 3
            }
        ]
    }
]

export interface IRole {
    roleComment: string
    roleID: number;
    roleName: string
}

@Injectable({
    providedIn: 'root'
})
export class RolesService {

    constructor(
        private _http: HttpClient
    ) {
    }

    // загрузка списка ролей
    async loadRoles(): Promise<IRole[]> {
        return this._http.get<IRole[]>(`api/Roles`)
            .toPromise()
    }

}