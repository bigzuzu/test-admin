import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './Auth.service';

export interface IUser {
    rfID: number;
    userID: number;
    userFioAll: string;
    userFioShort: string;
    filialID: number;
    filialNameAll: string;
    filialNameShort: string;
    cityAllName: string;
    roleIDs: string;
    roleNames: string;
}

export interface ITransformedUsers {
    roleIDs: Array<number>
    rfID: number;
    userID: number;
    userFioAll: string;
    userFioShort: string;
    filialID: number;
    filialNameAll: string;
    filialNameShort: string;
    cityAllName: string;
    roleNames: string;
}

export interface IUserWithRoles extends IUser {
    roles: boolean[]
}

export interface IUserForm {
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    middleName: string;
    dateBirth: string | Date;
    phone: string;
    phoneHome: string;
    email: string;
    docSerial: string;
    docNumber: string;
    filialId: number;
    role: number;
}

export const defaultUserForm = {
    login: '',
    password: '',
    firstName: '',
    lastName: '',
    middleName: '',
    dateBirth: '',
    phone: '',
    phoneHome: '',
    email: '',
    docSerial: '',
    docNumber: '',
    filialId: null,
    role: null
}

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(
        private _http: HttpClient,
        private authService: AuthService
    ) { }

    // загрузка списка пользователей с фильтрацией по подразделению
    async loadUsers(userId: number, filialID: number): Promise<ITransformedUsers[]> {
        return this._http.get<IUser[]>(`api/UsersOfFilial?UserID=${userId}&FilialID=${filialID}`)
            .toPromise()
            .then((response) => {
                // так как бэкендщик клянется что C# не умеет возвращать массивы, преобразовываем список ролей в массив на фронте))
                let transformedUsers: ITransformedUsers[] = [];
                response.forEach((item) => {
                    transformedUsers.push({
                        rfID: item.rfID,
                        userID: item.userID,
                        userFioAll: item.userFioAll,
                        userFioShort: item.userFioShort,
                        filialID: item.filialID,
                        filialNameAll: item.filialNameAll,
                        filialNameShort: item.filialNameShort,
                        cityAllName: item.cityAllName,
                        roleIDs: this.parseToArray(item.roleIDs.toString()),
                        roleNames: item.roleNames
                    })
                });
                return transformedUsers;
            })
    }

    parseToArray(data: string): Array<number> {
        let parsedArray = [];
        if(data) {
            parsedArray = data.slice(1).split(',', ).map((item) => parseInt(item))
        } 
        return parsedArray
    }

    // добавление пользователя
    addUser(form: IUserForm, date: string) {
        return this._http.get<any>(`api/UserRegister?UserLogin=${form.login}&UserPass=${form.password}&FirstName=${form.firstName}&LastName=${form.lastName}&MiddleName=${form.middleName}&DateBirth=${date}&PhoneMob=${form.phone}&PhoneHome=${form.phoneHome}&Email=${form.email}&DocSerial=${form.docSerial}&DocNumber=${form.docNumber}&DocTypeID=1&CurUserID=${this.authService.user.userID}&CurFilialID=${this.authService.user.filialID}`)
                                    .toPromise()
    }

    // задаем пользователю подразделение и роль
    setRoleAndFilial(filial: number, user: number, role: number) {
        return this._http.get<any>(`api/UserSetFilial?UserID=${user}&FilialID=${filial}&RoleID=${role}&CurUserID=${this.authService.user.userID}&CurFilialID=${this.authService.user.filialID}`)
            .toPromise()
    }

    deleteRole(filial: number, user: number, role: number) {
        return this._http.get<any>(`api/UserRemoveRole?UserID=${user}&FilialID=${filial}&RoleID=${role}&CurUserID=${this.authService.user.userID}&CurFilialID=${this.authService.user.filialID}`)
            .toPromise()
    }
    
}