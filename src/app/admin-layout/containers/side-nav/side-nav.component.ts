import { Component, OnInit } from "@angular/core";
import { FilialsService } from '@services/Filials.service';

@Component({
    selector: 'side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss']
})

export class SideNavComponent implements OnInit {
    public filials = [];

    constructor(private filialsService: FilialsService) {}

    async ngOnInit() {
        let filialsList = await this.filialsService.loadFilials()
        this.filials = filialsList.map((item) => ({name: item.filialNameShort, id: item.filialID}));
        this.filials.unshift({name: 'Все', id: 0})
    }
}