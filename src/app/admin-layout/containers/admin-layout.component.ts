import { Component } from "@angular/core"
import { AuthService } from '@services/Auth.service';

@Component({
    selector: 'admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.scss']
})

export class AdminLayoutComponent {
    constructor(
        private authService: AuthService
    ){}
    
    public logout() {
        this.authService.logout()
    }
}
